import React from "react";

const ImageBrowser = ({ year, Url }) => {
    return (
        <div className="img-container">
            <p className="year">This is the Finnish nature picture {year}</p>
            <img src={Url} alt="Finnish-Nature-2021"/>
        </div>
    )
}

export default ImageBrowser