import React from 'react';
import './App.css';
import ImageBrowser from './ImageBrowser';

function App() {
  return (
    <div className="App">
        <ImageBrowser year={2021} Url="http://www.vuodenluontokuva.fi/vlk/userfiles/vlk2021/sarjavoittajat/e.1._liskomies_pekka%20tuuri.jpg" />
    </div>
  );
}

export default App;
